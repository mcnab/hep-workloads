# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

function generateSummary(){
  #-----------------------
  # Generate json (bash)
  #-----------------------
  # Generate the json summary
  echo -e "\n[parseResults] generate old json summary (from bash parser)"
  local app="\"UNKNOWN\""
  if [ -f $BMKDIR/version.json ]; then app=$(jq --arg flavor $flavor '. + { "containment" : $flavor }' $BMKDIR/version.json); fi
  local OUTPUT=${APP}_summary_old.json
  echo -e "{
    \"run_info\": {\"copies\":$NCOPIES , \"threads_per_copy\":1 , \"events_per_thread\" : $NEVENTS_THREAD },
    \"report\": {$resJSON , \"log\": \"${s_msg}\"}, 
    \"app\":${app} }" > $OUTPUT
  cat $OUTPUT

  #-----------------------
  # Generate json (python)
  #-----------------------
  # Generate the json summary
  echo -e "\n[parseResults] generate json summary (from python parser)"
  local OUTPUT2=${APP}_summary.json
  echo -e "{
    \"run_info\": {\"copies\":$NCOPIES , \"threads_per_copy\":1 , \"events_per_thread\" : $NEVENTS_THREAD }, 
    \"report\": {$resJSON2 , \"log\": \"${s_msg}\"}, 
    \"app\":${app} }" > $OUTPUT2
  cat $OUTPUT2
}

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# [NB: if a separate function generateSummary exists, it must be internally called by parseResults]
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# Return value: please return 0 if parsing was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[parseResults] ERROR! Invalid arguments '$@' to parseResults"; return 1; fi
  echo "[parseResults] parse results and generate summary (previous status: $1)"
  echo "[parseResults] current directory: $(pwd)"
  export resJSON='"wl-scores":{}'
  export resJSON2='"wl-scores":{}'
  export s_msg="ok"
  if [ "$1" -ne 0 ]; then
    echo "Previous steps failed: skip parsing, go to generateSummary"
    s_msg="ERROR"
    generateSummary # this has no return code
    return 1
  fi
  #-----------------------
  # Parse results (bash)
  #-----------------------
  echo -e "\n[parseResults] bash parser starting"
  echo "[parseResults] parsing results from" proc_*/out_*.log
  local failedParse=0
  local s_msg="ok"
  local resJSON=`grep "EVENT LOOP" proc_*/out_*.log  | \
                   awk -F"|" 'BEGIN{amin=1000000;amax=0;count=0;}  \
                              { val=1000.*$5/$6; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} \
                             END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1])/ 2.0;}; 
printf "\"wl-scores\": {\"gen-sim\": %.4f} , \"wl-stats\": {\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}",
sum, sum/count, median, amin, amax, count
}' || (echo "\"wl-scores\":{}"; exit 1)`
  shstatus=$?
  [ "$shstatus" != "0" ] && s_msg="ERROR in bash parsing"
  echo $resJSON
  echo "[parseResults] bash parser completed (status=$shstatus)"
  #-----------------------
  # Parse results (python)
  #-----------------------
  echo -e "\n[parseResults] python parser starting using $(python3 -V &> /dev/stdout)"
  local resJSON2 # declare 'local' separately to avoid masking $? (https://stackoverflow.com/a/4421282)
  resJSON2=$(PYTHONPATH=${parseResultsDir} python3 -c "from parseResults import *; parseBmkDir('.')") # same directory as parseResults.sh
  pystatus=$?
  [ "$pystatus" != "0" ] && s_msg="ERROR in python parsing"
  echo $resJSON2
  echo "[parseResults] python parser completed (status=$pystatus)"
   #-----------------------
  # Generate summary
  #-----------------------
  generateSummary # this has no return code
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $shstatus
}

