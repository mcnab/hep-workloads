#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-RECO-specific setup
  release=21.0.77 
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  source $AtlasSetup/scripts/asetup.sh Athena,${release}
  # Configure WL copy
  unset FRONTIER_SERVER
  export ATHENA_PROC_NUMBER=$NTHREADS
  # Execute WL copy
  #Reco_tf.py --AMI q221 --conditionsTag 'default:OFLCOND-MC16-SDR-14' --DBRelease '200.0.3' --maxEvents "$(($NEVENTS_THREAD*$NTHREADS))" > out_$1.log 2>&1 
  Reco_tf.py --conditionsTag 'default:OFLCOND-MC16-SDR-14' --DBRelease '200.0.3' --ignoreErrors 'False' --autoConfiguration everything --maxEvents "$(($NEVENTS_THREAD*$NTHREADS))" --digiSeedOffset2 1 --inputHITSFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/Tier0ChainTests/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.simul.HITS.e6337_s3126/HITS.12860054._032508.pool.root.1' --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(0.);from LArROD.LArRODFlags import larRODFlags;larRODFlags.nSamples.set_Value_and_Lock(4);from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet="AODFULL"' 'HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.overrideMetadata+=["PhysicsList"];' 'RAWtoESD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.triggerMenuSetup="MC_pp_v7";from CaloRec.CaloCellFlags import jobproperties;jobproperties.CaloCellFlags.doLArCellEmMisCalib=False;' --digiSeedOffset1 1 --steering 'doRDO_TRIG' --DataRunNumber '284500' --outputRDOFile="myRDO.pool.root" --outputESDFile="myESD.pool.root" --outputAODFile="myAOD.pool.root" > out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}




function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi


  # Return 0 if input arguments are valid, 1 otherwise
  # Report any issues to parseResults via s_msg
  export s_msg="ok"
  tot_load=$(($NCOPIES*$NTHREADS))
  if [ $tot_load -gt `nproc` ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load > number of available cores (`nproc`)"
    echo $s_msg
    return 1
  elif [ $tot_load -eq 0 ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load. Please fix it"
    echo $s_msg
    return 1
  elif [ $tot_load -ne `nproc` ];
  then s_msg="[WARNING] NCOPIES*NTHREADS ($NCOPIES*$NTHREADS=$tot_load) != `nproc` (number of available cores nproc)"
    echo $s_msg
  fi
  if [  "$NTHREADS" -eq 1  ];
  then s_msg="[WARNING] NTHREADS=1 , running Athena not in MP mode"
    echo $s_msg
  fi

  return 0
}






# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark

NTHREADS=4 
NCOPIES=$((`nproc`/$NTHREADS))
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > `nproc`
  NCOPIES=1
  NTHREADS=$((`nproc`/$NCOPIES))
fi
NEVENTS_THREAD=10


# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
