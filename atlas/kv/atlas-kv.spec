HEPWL_BMKEXE=atlas-kv-bmk.sh
HEPWL_BMKOPTS="-c 1 -e 100"
HEPWL_BMKDIR=atlas-kv
HEPWL_BMKDESCRIPTION="ATLAS KV: GEANT4 simulation of 100 single muon events, based on Athena version v17.8.0.9"
HEPWL_BMKANNOUNCE=false
HEPWL_DOCKERIMAGENAME=atlas-kv-bmk
HEPWL_DOCKERIMAGETAG=ci2.0
HEPWL_CVMFSREPOS=atlas.cern.ch
