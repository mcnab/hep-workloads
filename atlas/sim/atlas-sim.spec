HEPWL_BMKEXE=atlas-sim-bmk.sh 
HEPWL_BMKOPTS="-e 3 -t 2"
HEPWL_BMKDIR=atlas-sim
HEPWL_BMKDESCRIPTION="ATLAS Sim (GEANT4) based on athena version 21.0.15"
HEPWL_DOCKERIMAGENAME=atlas-sim-bmk
HEPWL_DOCKERIMAGETAG=v2.1
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,sft.cern.ch
